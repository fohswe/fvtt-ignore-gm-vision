/*
This code adapted from KaKaRoTo's The Furnace module: https://github.com/League-of-Foundry-Developers/fvtt-module-furnace
*/

class IgnoreGmVision {
    static init() {
        game.settings.register("ignore-gm-vision-mod", "tokenIgnoreVision", {
            name: game.i18n.localize("IGNORE_GM_VISION.ACTORS.tokenIgnoreVisionGM"),
            scope: "world",
            config: false,
            default: false,
            type: Boolean,
            onChange: value => {
                canvas.initializeSources()
            }
        });
    }

    static getSceneControlButtons(buttons) {
        let tokenButton = buttons.find(b => b.name == "token")

        if (tokenButton) {
            tokenButton.tools.push({
                name: "vision",
                title: game.i18n.localize("IGNORE_GM_VISION.ACTORS.tokenIgnoreVisionGM"),
                icon: "far fa-eye-slash",
                toggle: true,
                active: game.settings.get("ignore-gm-vision-mod", "tokenIgnoreVision"),
                visible: game.user.isGM,
                onClick: (value) => game.settings.set("ignore-gm-vision-mod", "tokenIgnoreVision", value)
            });
        }
    }

    static setup() {
        // Disable sight layer's token vision if GM and option enabled
        let getterProperty = Object.getOwnPropertyDescriptor(SightLayer.prototype, 'tokenVision'); //Retrieve value of tokenVision
        if (getterProperty == undefined)
            return false;
        Object.defineProperty(SightLayer.prototype, "__IGNORE_GM_VISION__original__tokenVision", getterProperty); //Set this modules own tokenVision property to the original value
        Object.defineProperty(SightLayer.prototype, "tokenVision", { get: function() {
            if (game.user.isGM && game.settings.get("ignore-gm-vision-mod", "tokenIgnoreVision"))
                return false;
            return Object.getOwnPropertyDescriptor(SightLayer.prototype, "__IGNORE_GM_VISION__original__tokenVision");
        }
        });
        return true;
    }
}

Hooks.on('init', IgnoreGmVision.init)
Hooks.on('getSceneControlButtons', IgnoreGmVision.getSceneControlButtons)
Hooks.on('setup', IgnoreGmVision.setup)