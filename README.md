# FVTT Ignore GM Vision
Adds a toggle button to ignore GM vision in Foundry VTT. This code was broken out and refactored from The Furnace module.

# Attribution
## Ignore GM Vision
Original code from KaKaRoTo's The Furnace module https://github.com/League-of-Foundry-Developers/fvtt-module-furnace (Licensed under CC BY 4.0)

# License
This code is released under MIT, as well as FVTT's Module Development License: https://foundryvtt.com/article/license/
